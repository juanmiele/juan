﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Carta
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        private int palo;
        public int Palo
        {
            get { return palo; }
            set { palo = value; }
        }
        private int imagen;

        public int Imagen
        {
            get { return imagen; }
            set { imagen = value; }
        }

        private int ubicacion;

        public int Ubicacion
        {
            get { return ubicacion; }
            set { ubicacion = value; }
        }

        private int pocision;

        public int Pocision
        {
            get { return pocision; }
            set { pocision = value; }
        }



    }
}
