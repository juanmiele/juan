﻿
using BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Jugador
    {
        public bool robarDelMazo(BE.Mazo mazo, BE.Jugador jug)
        {
            bool error = false;
            if (mazo.CARTAS.Count() >= 1)
            {
                mazo.CARTAS[0].Pocision = jug.MANO.CARTAS.Count();
                mazo.CARTAS[0].Ubicacion = jug.Numero;
                jug.MANO.CARTAS.Add(mazo.CARTAS[0]);
                mazo.CARTAS.RemoveAt(0);
            }
            else
            {
                error = true;
            }
            jug.MANO.CARTAS = this.ordenarMano(jug.MANO.CARTAS);

            return error;
        }

        private List<BE.Carta> ordenarMano(List<BE.Carta> manoAux)
        {
            List<BE.Carta> orden = new List<BE.Carta>();
            int x = 0;
            foreach (BE.Carta carta in manoAux)
            {
    
                carta.Pocision = x;
                x++;

            }

            return manoAux;
        }

        //public micarta mostrarcartas(BE.Jugador jugador, int x, int y)
        //{
        //    foreach (BE.Carta carta in jugador.MANO.CARTAS)
        //    {
        //        x = x + 110;
        //        micarta mostrarcarta = new micarta();
        //        mostrarcarta. = new Point(x, y);
        //        mostrarcarta.establecerimagen(carta.Imagen);
        //        this.Controls.Add(mostrarcarta);
        //        return mostrarcarta;
        //    }


        //}

        public List<BE.Carta> inicio(BE.Mazo mazo, BE.Jugador jug)
        {
            List<BE.Carta> cartas = new List<BE.Carta>();

            for (int m = 0; m < 3; m++)
            {

                mazo.CARTAS[0].Pocision = m;
                mazo.CARTAS[0].Ubicacion = jug.Numero;
                cartas.Add(mazo.CARTAS[0]);
                mazo.CARTAS.RemoveAt(0);


            }
            return cartas;
        }
        //public BE.Jugador inicio(BE.Mazo mazo, BE.Jugador jugador)
        //{

        //    for (int m = 0; m < 3; m++)
        //    {
        //        jugador.MANO.CARTAS.Add(mazo.CARTAS[0]);
        //        mazo.CARTAS.RemoveAt(0);

        //    }
        //    jugador.PUNTOSPARTIDA = 0;
        //    return jugador;
        //}




        public List<BE.Carta> Ordenar(List<BE.Carta> manoAux)
        {
            List<BE.Carta> orden = new List<BE.Carta>();
            int x=0;
            foreach (BE.Carta carta in manoAux)
            {
                x++;
                carta.Pocision = x;

            }

            return orden;
        }
       

    }
}
