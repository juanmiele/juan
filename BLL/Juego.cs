﻿using BE;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BLL
{
    public class Juego
    {
        Mazo gestormazo = new Mazo();
        Mesa gestomeza = new Mesa();
        Turno gestorturno = new Turno();
        public Jugador gestorjugador = new Jugador();
        public BE.Mazo mazo = new BE.Mazo();
        public BE.Mesa mesa = new BE.Mesa();
        public static bool levanta;
        BE.Turno turno = new BE.Turno();


        private List<BE.Carta> cartas = new List<BE.Carta>();


        public List<BE.Carta> CartaSeleccionadas
        {
            get { return cartas; }
        }
        private List<BE.Carta> cartasjugador = new List<BE.Carta>();


        public List<BE.Carta> CartasJugador
        {
            get { return cartasjugador; }
        }
        private List<BE.Jugador> jugadores = new List<BE.Jugador>();

        public List<BE.Jugador> NumeroDeJugador
        {
            get { return jugadores; }
        }

        public void Limpiar()
        {
            cartas.Clear();
            levanta = false;
        }

        public void Inicio(int CantidadDeJugadores)
        {
            mazo.CARTAS = gestormazo.LlenarMazo();
            mesa.CARTAS = gestomeza.inicio(mazo);
            for (int m = 0; m < CantidadDeJugadores; m++)
            {
                BE.Jugador jugador = new BE.Jugador();
                jugador.Numero = m;
                jugador.MANO.CARTAS = gestorjugador.inicio(mazo, jugador);
                jugadores.Add(jugador);
                turno.JUGADORES.Add(jugador);
               
            }
            turno.INDICE= 0;
            cartasjugador = jugadores[0].MANO.CARTAS;
        }
        public void SeleccionarCarta(BE.Carta cartasmarcadas, bool valida)
        {


            if (valida == true)
            {
                CartaSeleccionadas.Add(cartasmarcadas);
            }
            else
            {
                CartaSeleccionadas.Remove(cartasmarcadas);

            }
        }

        public bool Sumar()
        {
            int total = 0;
            foreach (BE.Carta carta in CartaSeleccionadas)
            {
                total = total + carta.Numero;
            }
            if (total == 15)
            {
                levanta = true;
                foreach (BE.Carta carta in CartaSeleccionadas)
                {
                    mesa.CARTAS.Remove(carta);
                    NumeroDeJugador[turno.INDICE].MANO.CARTAS.Remove(carta);
                    NumeroDeJugador[turno.INDICE].MONTON.CARTAS.Add(carta);
                }
            }
            else
            {
                levanta = false;
                foreach (BE.Carta carta in CartaSeleccionadas)
                {
                    if (carta.Ubicacion == turno.INDICE)
                    {
                        NumeroDeJugador[turno.INDICE].MANO.CARTAS.Remove(carta);
                        //carta.Ubicacion = 8;
                        //carta.Pocision = mesa.CARTAS.Count();
                        mesa.CARTAS.Add(carta);

                    }
                }

            }
            gestorturno.SiguienteTurno(turno);
            gestorjugador.robarDelMazo(mazo, jugadores[turno.INDICE]);
            cartasjugador = jugadores[turno.INDICE].MANO.CARTAS;
            gestomeza.Ordenar(mesa.CARTAS);

            return levanta;
        }
    }
}
