﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Form1 : Form
    {

        public BLL.Juego gestorjuego = new BLL.Juego();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Inicio();
            
        }

        private void Inicio()
        {
            gestorjuego.Inicio(2);
        }
        private void Limpiar()
        {
            this.Controls.Clear();
            this.Controls.Add(button1);
            this.Controls.Add(button2);
            gestorjuego.CartaSeleccionadas.Clear();
            Mostrarcartas(gestorjuego.mesa.CARTAS);
            Mostrarcartas(gestorjuego.CartasJugador);
        }

              

        private void button1_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        public void Mostrarcartas(List<Carta> cartas)
        {
            foreach (Carta carta in cartas)
            {
                micarta mostrarcarta = new micarta(carta);
                mostrarcarta.CartaClick += Carta_CartaClick;
                this.Controls.Add(mostrarcarta);
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            if (gestorjuego.Sumar() == false)
            {
                Limpiar();
            }
            else
            {
                Limpiar();
            }
        }
        private void Carta_CartaClick(micarta micarta)
        {
                gestorjuego.SeleccionarCarta(micarta.Carta, micarta.Seleccionada);
        }
 

    }
}
